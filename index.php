<!DOCTYPE html>
<html>
<head>
	<title>Belajar OOP</title>
</head>
<body>
	<?php 
		require 'Animal.php';
		require 'frog.php';
		require 'ape.php';
		$sheep = new Animal("shaun");
		echo "Nama binatang: $sheep->name <br>";
		echo "Berkaki: $sheep->legs <br>";
		echo "Berdarah dingin: $sheep->cold_blooded <br><br>";
		$kodok = new Frog("buduk");
		echo "Nama binatang: $kodok->name <br>";
		echo "Berkaki: $kodok->legs <br>";
		echo "Berdarah dingin: $kodok->cold_blooded <br>";
		$kodok->jump() ;
		$sungokong = new Ape("kera sakti");
		echo "Nama binatang: $sungokong->name <br>";
		echo "Berkaki: $sungokong->legs <br>";
		echo "Berdarah dingin: $sungokong->cold_blooded <br>";
		$sungokong->yell()
	 ?>
</body>
</html>